# Taina:
Simple, easy to use password manager

## Introduction:

I need a simple password manager that relies on gpg Keys to encrypt the passwords

And use sqlite Database to save the Passwords,So I wrote this app

I had a hard time choosing a name for this app, In the end I chose taina as a name

taina is Roman word meaning secret,If you find a better name, please tell me

## Requirements:

This app is written using Ruby
so it needs Ruby

If you dont use linux

you Need to install gpg

## installation:
To install the app
Open your os terminal then clone the app code useing git
```bash
git clone https://gitlab.com/alimiracle/taina
```

then type

``` bash
cd taina
bundle install
```

## Environment variables:

Currently the app supports 2 environment variable to change its Default Behavior

- GPG_KEY
Used to select another GPG key

- TAINA_DB
Used to select another sqlite db

## Usage:
Just run the app from the command line without any arguments and it will give you all the commands

## Generating a new GPG key:
Taina use gpg keys to encrypt and decrypt passwords
So You must Generating a new GPG key
1. Download and install the GPG command line tools for your operating system. We generally recommend installing the latest version for your operating system.
2. Open The terminal
3. Generate a GPG key pair. Since there are multiple versions of GPG, you may need to consult the relevant man page to find the appropriate key generation command. Your key must use RSA.
If you are on version 2.1.17 or greater, paste the text below to generate a GPG key pair.
    ``` bash
    $ gpg --full-generate-key
    ```
     If you are not on version 2.1.17 or greater, the gpg --full-generate-key command doesn't work. Paste the text below and skip to step 6.
    ``` bash
    $ gpg --default-new-key-algo rsa4096 --gen-key
    ```
4. At the prompt, specify the kind of key you want, or press Enter to accept the default.
5. At the prompt, specify the key size you want, or press Enter to accept the default. Your key must be at least 4096 bits.
6. Enter the length of time the key should be valid. Press Enter to specify the default selection, indicating that the key doesn't expire.
7. Verify that your selections are correct.
8. Enter your user ID information.
9. Type a secure passphrase.
10. Use the ``` bash $gpg --list-secret-keys --keyid-format=long ``` command to list the long form of the GPG keys for which you have both a public and private key.
## Getting Help:
So you need help.

People can help you, but first help them help you, and don't waste their time.

Provide a complete description of the issue.

If it works on A but not on B and others have to ask you: "so what is different between A and B" you are wasting everyone's time.

"hello", "please" and "thanks" are not swear words.
## License:
gpl v3+
